FireBeetle Covers - 24 8 LED Matrix 点阵屏

DFRobot FireBeetle萤火虫系列是专为物联网设计的低功耗微控制器。该系列产品，定位于低功耗物联网，旨在方便、快速的搭建物联网硬件平台。FireBeetle系列有三个大类，分别是Board（主板）、Covers（扩展）、Accessories（配件）。 FireBeetle LED点阵显示屏，支持低功耗外围硬件设计。模块使用FireBeetle兼容接口设计，上手简单，即插即用。采用HT1632C高性能LED驱动芯片，每个LED都有独立的寄存器，可以分别驱动每个LED灯。内置256KHz RC时钟，低功耗模式下RC时钟关闭，功耗仅5uA. LED 点阵屏支持16级PWM灯亮度调节。可以通过Arduino库显示字符，动态滚屏，画点、画线等。

![](./arduinoC/_images/featured.png)

# 产品简介

- 产品链接：https://www.dfrobot.com.cn/goods-1398.html

- 介绍：本扩展库为 DFRobot FireBeetle 24×8 LED点阵屏（白色）(DFR0484)设计，支持 Mind+ 导入库，要求 Mind+ 软件版本为 1.6.2 及以上。


# 积木

![](./arduinoC/_images/blocks.png)

 
 
示例程序

## 静止显示

![](./arduinoC/_images/example-静止显示.png)

## 滚动显示

![](./arduinoC/_images/example-滚屏显示.png)

## 任意显示

![](./arduinoC/_images/example-任意显示.png)


# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|uno||√|||
|micro:bit|||||
|mpython|||||
|arduinonano|||||
|leonardo|||||
|mega2560|||||


# 更新日志

V0.0.1 基础功能完成

