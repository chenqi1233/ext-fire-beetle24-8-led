//% color="#A0522D" iconWidth=50 iconHeight=40
namespace DFRobot_HT1632C{

    //% block="DFRobot_HT1632CInit CS [CS] DATA[DATA] WR [WR]  " blockType="command" 
    
     
     //% CS.shadow="dropdown" CS.options="CS"
     //% DATA.shadow="dropdown" DATA.options="DATA"
     //% WR.shadow="dropdown" WR.options="WR"

    export function DFRobot_HT1632CInit(parameter: any, block: any) {
    
        let cs = parameter.CS.code;
        let data = parameter.DATA.code;
        let wr = parameter.WR.code;

        Generator.addInclude("DFRobot_HT1632CInit", "#include <DFRobot_HT1632C.h>");
        
        Generator.addObject("DFRobot_HT1632CInit","DFRobot_HT1632C",`ht1632c =DFRobot_HT1632C(${data},${wr},${cs});`);
  
        Generator.addCode("ht1632c.begin();"); 
        Generator.addCode("ht1632c.isLedOn(true);");
        Generator.addCode("ht1632c.clearScreen();");
        Generator.addCode("ht1632c.setCursor(0,0);");
    }
  
    //% block="DFRobot_HT1632CInitwrite  STR [STR] TIME[TIME]  ACTION[ACTION] " blockType="command" 
    //% STR.shadow="string" STR.defl="hello"
    //% TIME.shadow="number" TIME.defl="50"
    //% ACTION.shadow="dropdown" ACTION.options="ACTION"
    
    export function DFRobot_HT1632CInitwrite(parameter: any, block: any) { 
        let str=parameter.STR.code;
        let time=parameter.TIME.code;
        let action=parameter.ACTION.code;

          if(`${action}` === 'A'){
        Generator.addCode(`ht1632c.print(${str});`);
        

     } else if(`${action}` === 'B'){
        Generator.addCode(`ht1632c.print(${str},${time});`);
    
   }
}
        //% block="---"
    export function noteSep() {

    } 
    //% block="DFRobot_HT1632CInitwrite1  X [X]  Y[Y]" blockType="command" 
    //% X.shadow="range" X.params.min="1"  X.params.max="24" 
    //% Y.shadow="range" Y.params.min="1"  Y.params.max="8" 
    export function DFRobot_HT1632CInitwrite1(parameter: any, block: any) { 
        let x=parameter.X.code;
        let y=parameter.Y.code;
        Generator.addCode(`ht1632c.setPixel(${x},${y});`);
        Generator.addCode(`ht1632c.writeScreen();`); 
   }


  // //% extenralFunc
  //   export function DFRobot_HT1632CInitwrite2() {
  //       return [
  //           {matrix: "111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000"}
         
  //       ]
  //   }
  //    //% block="BadgeDisplay Matrix [MT] At X[X]" blockType="command"
  //   //% MT.shadow="matrix" MT.params.row=8 MT.params.column=24 MT.defl="111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000111111111111111111111111000000000000000000000000"
  //   //% X.shadow="range" X.params.min="1" X.params.max="44" X.defl="1"
  //   //% MT.params.builtinFunc="DFRobot_HT1632CInitwrite2"
  //   export function setMatrix(parameter: any, block: any) {
  //       let matrix = parameter.MT.code;
         
  //       Generator.addObject("addhz_matrix","uint16_t",`hz_matrix[24];`);
  //       let hz_matrix:any[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  //       for (let i = 0; i < 24; i++) {
  //           for (let j = 0; j < 8; j++) {
  //               hz_matrix[i] += matrix.charAt(i + 8 * j) << (7 - j);
  //           }
  //           if (hz_matrix[i] < 0x10) {
  //               hz_matrix[i] = "0x00" + hz_matrix[i].toString(16);
  //           } else if (hz_matrix[i] < 0x100) {
  //               hz_matrix[i] = "0x0" + hz_matrix[i].toString(16);
  //           } else {
  //               hz_matrix[i] = "0x" + hz_matrix[i].toString(16);
  //           }
  //           if (i % 2) 
  //               Generator.addCode(`hz_matrix[${i-1}] = ${hz_matrix[i-1]};hz_matrix[${i}] = ${hz_matrix[i]};`);
  //           else if (i == 10) 
  //               Generator.addCode(`hz_matrix[${i}] = ${hz_matrix[i]};`);
  //       }
  //       let x = parameter.X.code;
  //       Generator.addCode(`ht1632c.SendMatrix(hz_matrix, ${x});`);
  //   }
     
}